using System.ComponentModel.DataAnnotations;

namespace hello_blog_api.Models
{
    public class BlogPostModelCreate
    {
        public string Label { get; set; }
        
        [Required]
        public string Title { get; set; }
        
        [Required]
        public string Content { get; set; }
    }
}