using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using System.Net;
using System.Collections.Generic;
using hello_blog_api.Repository;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostController: Controller
    {
        private readonly BlogDbContext _dbContext;
        private readonly IMapper _mapper;

        public BlogPostController(BlogDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        
        ///<summary>
        ///Creates a blog using the payload information.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateBlogPost([FromBody] BlogPostModelCreate blogPost)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Model is not valid!");
                }

                BlogPost entity = _mapper.Map<BlogPost>(blogPost);

                var createdEntity = await _dbContext.BlogPosts
                    .AddAsync(entity);
                var savedItemsCount = await _dbContext.SaveChangesAsync();
                
                if (savedItemsCount != 1)
                {   
                     return StatusCode((int)HttpStatusCode.InternalServerError);
                }
                
                return CreatedAtAction(nameof(CreateBlogPost), createdEntity.Entity);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of all blogs.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAllBlogPostsAsync()
        {
            try
            {
                // Using old-fashioned manual mapping
                // List<BlogPostModel> blogPosts = await _dbContext.BlogPosts
                //     .Select(x => new BlogPostModel(){ 
                //         Id = x.Id,
                //         Content = x.Content,
                //         Title = x.Title
                //         })
                //     .ToListAsync();

                // using automapper with method syntax
                // List<BlogPostModel> blogPosts = await _dbContext.BlogPosts
                //     .Select(blogPost => _mapper.Map<BlogPostModel>(blogPost))
                //     .ToListAsync();

                // using automapper with query syntax
                List<BlogPostModel> blogPosts = await (
                    from blogPost in _dbContext.BlogPosts
                    select _mapper.Map<BlogPostModel>(blogPost))
                    .ToListAsync();
                    
                var linqQuery = _dbContext.BlogPosts
                     .Select(blogPost => _mapper.Map<BlogPostModel>(blogPost));
                var linqQueryAsSQLCommand = linqQuery.ToQueryString();

                return Ok(blogPosts);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public IActionResult GetBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                // BlogPostModel blogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                // if (blogPost == null)
                // {
                //     return StatusCode((int)HttpStatusCode.NotFound);
                // }
                // return Ok(blogPost);
                return Ok(null);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}